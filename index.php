<?php

if (!isset($_GET['action'])) {
    $_GET['action'] = 'index';
}
$fileExist = file_exists(dirname(__FILE__) . '/includes/' . $_GET['action'] . '-action.php');
if (!$fileExist) {
    http_response_code(404);
}
//var_dump($_GET);exit();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Web Developper, Analyst Programmer, IT Engineer - SylvainMerlinPro.com</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta content="My name is Sylvain MERLIN and I'm a IT Engineer in the making. I'll be graduating by November and I'll be looking for a new Working Experience!" name="description">
        <meta content="Sylvain Merlin" name="author">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php if ($isFile) { ?> <link rel="canonical" href="/<?php echo $_GET['action']; ?>/" /><?php } ?>
        <link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand" href="/">SylvainMerlinPro.com</a>
                    <ul class="nav">
                        <li <?php echo ($_GET['action'] == 'index') ? 'class="active" ' : ''; ?>><a href="/">Home</a></li>
                        <li <?php echo ($_GET['action'] == 'my-resume') ? 'class="active" ' : ''; ?>><a href="/my-resume/">My Résumé</a></li>
                        <li <?php echo ($_GET['action'] == 'shared-links') ? 'class="active" ' : ''; ?>><a href="/shared-links/">Shared Links</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown"href="#">My Network  <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="http://beknown.com/sylvain-merlin" target="_blank"><img src="/img/beknown-menu.png" alt="See my profile on BeKnown - SylvainMerlinPro.com" title="See my profile on BeKnown" height="25" width="25" />BeKnown</a></li>
                                <li><a href="fr.linkedin.com/in/sylvainmerlin/en" target="_blank"><img src="/img/linkedin-menu.png" alt="See my profile on LinkedIn - SylvainMerlinPro.com" title="See my profile on LinkedIn" height="25" width="25" />LinkedIn</a></li>
                                <li><a href="http://fr.viadeo.com/fr/profile/merlin.sylvain" target="_blank"><img src="/img/viadeo-menu.png" alt="See my profile on Viadeo - SylvainMerlinPro.com" title="See my profile on Viadeo" height="25" width="25" />Viadeo</a></li>
                                <li class="divider">Coding Network</li>
                                <li><a href="https://github.com/sylvainmerlin" target="_blank"><img src="/img/github-menu.png" alt="See my profile on GitHub - SylvainMerlinPro.com" title="See my profile on GitHub" height="25" width="25" />GitHub</a></li>
                                <li><a href="https://bitbucket.org/sylvainmerlin" target="_blank"><img src="/img/bitbucket-menu.png" alt="See my profile on BitBucket - SylvainMerlinPro.com" title="See my profile on BitBucket" height="25" width="25" />BitBucket</a></li>
                            </ul>
                        </li>
                        <li <?php echo ($_GET['action'] == 'contact-me') ? 'class="active" ' : ''; ?>><a href="/contact-me/">Contact Me</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="wrap">
            <div class="main container">
                <?php
                if ($fileExist == true) {
                    include dirname(__FILE__) . '/includes/' . $_GET['action'] . '-action.php';
                } else {
                    include dirname(__FILE__) . '/includes/error-action.php';
                }
                ?>
                <div class="row">
                    <div class="span4">
                        <h2><a href="/shared-links/">Shared Links</a></h2>
                        <p>
                            Watch the links I share on <a target="_blank" href="https://delicious.com/sylvainmerlin">Delicious</a>:
                        <p>
                            <?php
                            $key = 'B06xDIFJoTG5cA8b_4sOkvs-JWKYyi2OuySszoF-dk0=';
                            $curl = curl_init();
                            curl_setopt($curl, CURLOPT_URL, 'https://feeds.delicious.com/v2/json/sylvainmerlin?count=5&private=' . $key);
                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curl, CURLOPT_COOKIESESSION, true);
                            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                            $return = json_decode(curl_exec($curl));
                            curl_close($curl);
                            ?>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>URL</th>
                                    <th><i class="icon-tags"></i> Tags</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($return as $key => $val) {
                                    ?>
                                    <tr>
                                        <td><a href="<?php echo $val->u; ?>" target="_blank" data-placement="right" data-original-title="My comment:" data-content="<?php echo $val->n;?>" rel="popover" class="popinover"><?php echo $i; ?></a></td>
                                        <td><a href="<?php echo $val->u; ?>" target="_blank"><?php echo strlen($val->d) > 30 ? substr($val->d, 0, 25) . '...' : $val->d; ?></a></td>
                                        <td>
                                            <?php foreach ($val->t as $tKey) { ?>
                                            <a href="/shared-links/<?php echo strtolower(str_replace(' ','-',$tKey)); ?>/"><span class="label label-info"><?php echo $tKey; ?></span></a>
                                            <?php }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php $i++;
                                } ?>
                            </tbody>
                        </table>
                        <p class="right"><a href="/shared-links/"><i class="icon-chevron-right"></i> View more shared Links</a></p>
                    </div>
                    <div class="span4">
                        <h2><a href="/my-resume/#computer-skills">Computer Skills</a></h2>
                        <p>
                            See what I can bring to a company:
                        <p>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><strong>Skills</strong></th>
                                    <th><i class="icon-check"></i> Grades</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong>Java</strong></td>
                                    <td><div class="progress progress-warning progress-striped"><div id="java-bar" class="bar">70%</div></div></td>
                                </tr>
                                <tr>
                                    <td><strong>Javascript</strong> / <strong>JQuery</strong></td>
                                    <td><div class="progress progress-success progress-striped"><div id="javascript-bar" class="bar">75%</div></div></td>
                                </tr>
                                <tr>
                                    <td><strong>PHP</strong></td>
                                    <td><div class="progress progress-success progress-striped"><div id="php-bar" class="bar">90%</div></div></td>
                                </tr>
                                <tr>
                                    <td><strong>UML Modeling</strong></td>
                                    <td><div class="progress progress-success progress-striped"><div id="uml-bar" class="bar">80%</div></div></td>
                                </tr>
                                <tr>
                                    <td><strong>Zend Framework</strong></td>
                                    <td><div class="progress progress-success progress-striped"><div id="zend-bar" class="bar">80%</div></div></td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="right"><a href="/my-resume/#computer-skills"><i class="icon-chevron-right"></i> View more in my Résumé</a></p>
                    </div>
                    <div class="span4">
                        <div class="network">
                            <h2>My Network</h2>
                            <p>
                                You can find me on these professional Networks:
                            </p>
                            <ul class="simple-list">
                                <li class="span2"><a href="http://beknown.com/sylvain-merlin" target="_blank"><img src="/img/beknown.png" alt="Find me on BeKnown - SylvainMerlinPro.com" title="Find me on BeKnown" height="51" width="50" class="img-polaroid" /><strong>BeKnown</strong></a></li>
                                <li class="span2"><a href="http://fr.linkedin.com/in/sylvainmerlin/en" target="_blank"><img src="/img/linkedin.png" alt="Find me on LinkedIn - SylvainMerlinPro.com" title="Find me on LinkedIn" height="51" width="50" class="img-polaroid" /><strong>LinkedIn</strong></a></li>
                                <li class="span2"><a href="http://fr.viadeo.com/fr/profile/merlin.sylvain" target="_blank"><img src="/img/viadeo.png" alt="Find me on Viadeo - SylvainMerlinPro.com" title="Find me on Viadeo" height="51" width="50" class="img-polaroid" /><strong>Viadeo</strong></a></li>
                            </ul>
                        </div>
                        <div class="network">
                            <h2><a href="https://github.com/sylvainmerlin" target="_blank">Social Coding</a></h2>
                            <p>
                                Follow my latest projects on GitHub and BitBucket:
                            <p>
                            <ul class="simple-list">
                                <li class="span2"><a href="https://github.com/sylvainmerlin" target="_blank"><img src="/img/github.png" alt="Find me on GitHub - SylvainMerlinPro.com" title="Find me on GitHub" height="51" width="50" class="img-polaroid" /><strong>GitHub</strong></a></li>
                                <li class="span2"><a href="https://bitbucket.org/sylvainmerlin" target="_blank"><img src="/img/bitbucket.png" alt="Find me on BitBucket - SylvainMerlinPro.com" title="Find me on BitBucket" height="51" width="50" class="img-polaroid" /><strong>BitBucket</strong></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="muted credit">
                    Design made with <a href="http://twitter.github.io/bootstrap/index.html" target="_blank" >Twitter Bootstrap</a>
                </p>
            </div>
        </footer>

        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="/lib/waypoints/waypoints.min.js"></script>
        <script src="/script.js"></script>
    </body>
</html>

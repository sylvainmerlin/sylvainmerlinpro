/**
 * Set the waypoint plugin's parameters
 */
$.fn.waypoint.defaults = {
  context: window,
  continuous: true,
  enabled: true,
  horizontal: false,
  offset: $(this).height(),
  triggerOnce: false
}

/**
 * Load footer's progression bar
 */
$('#java-bar').waypoint(function(direction) {
    $('#java-bar').animate({width:$('#java-bar').html()}  ,350 , 'swing' );
    $('#javascript-bar').animate({width:$('#javascript-bar').html()}  ,350 , 'swing' );
    $('#php-bar').animate({width:$('#php-bar').html()}  ,350 , 'swing' );
    $('#uml-bar').animate({width:$('#uml-bar').html()}  ,350 , 'swing' );
    $('#zend-bar').animate({width:$('#zend-bar').html()}  ,350 , 'swing' );
});

/**
 * Allow popinover classed element to pop
 */
$('.popinover').popover({ trigger: 'hover' });

/**
 * Remove paginations classes
 */
$('.pagination li a').click(
    function(){
        if(typeof($(this).parent().attr('class'))==='undefined' && $(this).parent().attr('class')!=='disabled'){
            // Set selected value
            var selectedValue = $(this);
            switch(selectedValue.html()){
                case '»':
                    selectedValue = $('.pagination ul li.active').next().find('a');
                    break;
                case '«':
                    selectedValue = $('.pagination ul li.active').prev().find('a');
                    break;
            }
            
            // Show expected lines
            // TODO
            $('#line-table tr').removeClass('hidden');
            $('#line-table tr').attr('class','hidden');
            $('#line-table tr').each(
                function(){
//                    console.log(this.id);
                }
            );
            
            
            // Deactivate first item
            $('.pagination li').removeAttr('class');
            if(selectedValue.html()==='1'){
                $('.pagination ul li:first').attr('class','disabled');
            }
            else if($('.pagination ul li:last').prev().find('a').id === selectedValue.id){
                $('.pagination ul li:last').attr('class','disabled');
            }
            
            // Activate the button's class
            $(selectedValue).parent().attr('class','active');
            
        }
    }
);
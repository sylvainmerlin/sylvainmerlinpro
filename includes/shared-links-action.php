<?php
    $key = 'B06xDIFJoTG5cA8b_4sOkvs-JWKYyi2OuySszoF-dk0=';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://feeds.delicious.com/v2/json/tags/sylvainmerlin');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_COOKIESESSION, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $curl2 = curl_init();
    if(isset($_GET['tag'])){
        curl_setopt($curl2, CURLOPT_URL, 'https://feeds.delicious.com/v2/json/sylvainmerlin/'.str_replace('-','+',$_GET['tag']).'?count=100&private=' . $key);
    }
    else{
        curl_setopt($curl2, CURLOPT_URL, 'https://feeds.delicious.com/v2/json/sylvainmerlin?count=100&private=' . $key);
    }
    curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl2, CURLOPT_COOKIESESSION, true);
    curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);

    $mh = curl_multi_init();
    curl_multi_add_handle($mh,$curl);
    curl_multi_add_handle($mh,$curl2);
    $active = null;
    // Exécute le gestionnaire
    do {
        $mrc = curl_multi_exec($mh, $active);
    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

    while ($active && $mrc == CURLM_OK) {
        if (curl_multi_select($mh) != -1) {
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
    }
    
    $returnTags = json_decode(curl_exec($curl));
    $returnLinks = json_decode(curl_exec($curl2));
    
    curl_multi_remove_handle($mh, $curl);
    curl_multi_remove_handle($mh, $curl2);
    curl_multi_close($mh);
    
?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3">
            <h1>SHARED LINKS <br /><?php echo isset($_GET['tag']) ? '<small>'. ucfirst(strtolower(str_replace('-',' ',$_GET['tag']))).'</small>' : ''; ?></h1>
            <p>I'm always looking for <strong>new technologies</strong> I could use for my projects.</p>
            <p>When I find something interesting, I always <strong>share</strong> it for my co-workers and myself on this <strong>social-bookmarking</strong> site.</p>
            <p>Here's a list of subjects watch:</p>
            <ul class="simple-list">
                <?php 
                foreach ($returnTags as $key => $val) {?> 
                <li>
                    <a href="/shared-links/<?php echo strtolower(str_replace(' ','-',$key)); ?>/"><span class="label label-info"><?php echo $key; ?></span></a>
                </li>
                <?php
                }
                $return=null;
                ?>
            </ul>
        </div>
        <div class="span9">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>URL</th>
                        <th><i class="icon-tags"></i> Tags</th>
                    </tr>
                </thead>
                <tbody id="line-table">
                    <?php
                    $i = 1;
                    foreach ($returnLinks as $key => $val) {
                        ?>
                        <tr id="line-<?php echo $i; ?>" <?php echo $i>10 ? 'class="hidden"' : ''; ?>>
                            <td><a href="<?php echo $val->u; ?>" target="_blank" data-placement="right" data-original-title="My comment:" data-content="<?php echo $val->n;?>" rel="popover" class="popinover"><?php echo $i; ?></a></td>
                            <td><a href="<?php echo $val->u; ?>" target="_blank"><?php echo strlen($val->d) > 75 ? substr($val->d, 0, 72) . '...' : $val->d; ?></a></td>
                            <td>
                                <?php foreach ($val->t as $tKey) { ?>
                                    <a href="/shared-links/<?php echo strtolower(str_replace(' ','-',$tKey)); ?>/"><span class="label label-info"><?php echo $tKey; ?></span></a>
                                <?php }
                                ?>
                            </td>
                        </tr>
                        <?php $i++;
                    } ?>
                </tbody>
            </table>
        </div>

    </div>
    <div class="pagination pagination-right">
        <ul>
            <li class="disabled">
                <a href="javascript: void(0)">&laquo;</a>
            </li>
            <?php for($j=1;$j<=round($i/10);$j++){ ?>
            <li <?php echo $j==1 ? 'class="active"' : ''; ?>>
                <a id="page-<?php echo $j;?>" href="javascript: void(0)"><?php echo $j; ?></a>
            </li>
            <?php }?>
            <li>
                <a href="javascript: void(0);">&raquo;</a>
            </li>
        </ul>
    </div>
</div>


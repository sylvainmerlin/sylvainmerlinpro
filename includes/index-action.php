<div class="hero-unit">
    <div class="media">
        <div class="pull-left">
            <p>
                <a class="pull-left" href="/my-resume/">
                    <img class="media-object pull-left" src="/img/profile-pic.jpg" alt="Profile picture - SylvainMerlinPro.com" title="It's me !" width="178" height="178"/>
                </a>
            </p>
        </div>
        <div class="media-body">
            <h1 class="media-heading">Sylvain <span>Merlin</span> pro <small>IT Engineer</small></h1>
            <p class="media">
                Hi! My name is <strong>Sylvain MERLIN</strong>, I'm 23 year old and I'm a French <strong>Developper</strong> / <strong>Project Manager</strong>.
                This November I'll graduate from a Master degree in Computer Engineering after almost three years of a study contract and I'll be looking for a <strong>job</strong> as an <strong>IT Engineer</strong>.
            </p>
            <p>
                I've always been passionnate about <strong>new technologies</strong> and more precisely about <strong>programming</strong> and I'm now looking forward to work for a company where I could use my knowledge and <strong>learn new things</strong>.
            </p>
            <p>
                <a class="btn btn-primary btn-large" href="/my-resume/">See my Résumé &raquo;</a>
            </p>
        </div>
    </div>
    
</div>

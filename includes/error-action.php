<div class="hero-unit">
    <div class="media">
        <div class="pull-left">
            <p>
                <a class="pull-left" href="/my-resume/">
                    <img class="media-object pull-left" src="/img/profile-pic.jpg" alt="Profile picture - SylvainMerlinPro.com" title="It's me !" width="178" height="178"/>
                </a>
            </p>
        </div>
        <div class="media-body">
            <h1 class="media-heading">Oops!<small> Seems like you're lost!</small></h1>
            <p class="media">
                Don't worry, you'll probably find what you were looking for in <a href="/my-resume/">my Résumé</a>!
            </p>
            <p>
               If you still can't find a way to resolve the problem, I suggest you use that button right under.
            </p>
            <p>
                <a class="btn btn-primary btn-large" href="/contact-me/">Contact me now &raquo;</a>
            </p>
        </div>
    </div>
</div>